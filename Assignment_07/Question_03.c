#include<stdio.h>

int matrix_1[100][100];
int matrix_2[100][100];
int r1,c1,r2,c2;
int i,j,k;


int main(){
	
	printf("Enter number of rows for matrix_1 : ");
	scanf("%d",&r1);
	printf("Enter number of columns for matrix_1 : ");
	scanf("%d",&c1);
	getMatrix_1(r1,c1,matrix_1);
	printf("Enter number of rows for matrix_2: ");
	scanf("%d",&r2);
	printf("Enter number of columns for matrix_2 : ");
	scanf("%d",&c2);
	getMatrix_2(r2,c2,matrix_2);
	multiplication(matrix_1,matrix_2);
	addition(matrix_1,matrix_2);
	return 0;
	
}
	
int getMatrix_1(int r1,int c1,int matrix_1[100][100]){
	printf("\n\t\tMatrix 1\n\n");	
	for(i=0;i<r1;i++){
		for(j=0;j<c1;j++){
			printf("Enter value for row %d - col %d : ",(i+1),(j+1));
			scanf("%d",&matrix_1[i][j]);
		}
	}
	printf("\n");

	for(i=0;i<r1;i++){
		printf("\t");
		for(j=0;j<c1;j++){
			printf("%d	",matrix_1[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
	
int getMatrix_2(int r2,int c2,int matrix_2[100][100]){
	printf("\n\t\tMatrix 2\n\n");	
	for(i=0;i<r2;i++){
		for(j=0;j<c2;j++){
			printf("Enter value for row %d - col %d : ",(i+1),(j+1));
			scanf("%d",&matrix_2[i][j]);
		}
	}
	printf("\n");

	for(i=0;i<r2;i++){
		printf("\t");
		for(j=0;j<c2;j++){
			printf("%d	",matrix_2[i][j]);
		}
		printf("\n");
	}
}	
	


int multiplication(int matrix_1[100][100],int matrix_2[100][100]){
	
	if(c1==r2){
		int mul[100][100];
		for( j = 0; j < c1 ; j++){
	        for( i = 0; i < r1; i++){
	        	for(k=0;k<c1;k++){
	            mul[i][j] += matrix_1[i][k]*matrix_2[k][j];	
				}
	        }
	    }
		printf("\nMultiplication = Matrix 1 * Matrix 2\n\n");
		for(i=0;i<r1;i++){
			printf("\t");
			for(j=0;j<c1;j++){
				printf("%d	",mul[i][j]);
			}
			printf("\n");
		}
	}else if(c2==r1){
		int mul[100][100];
		for( j = 0; j < c2 ; j++){
	        for( i = 0; i < r2; i++){
	        	for(k=0;k<c2;k++){
	            mul[i][j] += matrix_1[i][k]*matrix_2[k][j];	
				}
	        }
	    }
		printf("\nMultiplication = Matrix 1 * Matrix 2\n\n");
		for(i=0;i<r2;i++){
			printf("\t");
			for(j=0;j<c2;j++){
				printf("%d	",mul[i][j]);
			}
			printf("\n");
		}
	}else{
		printf("This type matrixes cannot be multiplied together");
	}
	printf("\n");
		
}

int addition(int matrix_1[100][100],int matrix_2[100][100]){
	
	if(r1==r2 && c1==c2){
			int addition[100][100];
			for(i=0;i<r1;i++){
				for(j=0;j<c1;j++){
					addition[i][j]=matrix_1[i][j]+matrix_2[i][j];
				}
			}
			printf("\nAddition = Matrix 1 + Matrix 2\n\n");
			for(i=0;i<r1;i++){
				printf("\t");
				for(j=0;j<c1;j++){
					printf("%d	",addition[i][j]);
				}
				printf("\n");
			}
		}else{
			printf("This type matrixes cannot be added together");
	}

		
}
