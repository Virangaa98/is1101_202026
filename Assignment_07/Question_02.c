#include <stdio.h>
int main() {
    char s[1000],ch;
    int count = 0;

    printf("Enter a string: ");
    gets(s);
    
	printf("Enter a character to find its frequency: ");
    scanf("%c",&ch);
	
	int i;
    for (i=0;s[i]!='\0';++i){
        if (ch == s[i])
        ++count;
    }
	if(count>1)
    	printf("Frequency of %c = %d", ch, count);
    else
    	printf("Frequency of %c = %d", ch, 0);
    return 0;
}