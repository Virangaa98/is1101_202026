#include<stdio.h>
#include<string.h>
struct Student{
	char name[50];
	char subject[50];
	float mark; 
};


struct Student getStudent(int i){
	struct Student student;
	
	printf("Enter the name of student %d : ",i);
	scanf("%s",student.name);
	
	printf("Enter the subject : ");
	scanf("%s",student.subject);
	
	printf("Enter the mark : ");
	scanf("%f",&student.mark);
	
	printf("\n");
	
	return student;
}


int main(){
	struct Student student[5];

	int i;
	for(i=0;i<5;i++){
		student[i]=getStudent(i+1);
	}
	printf("\n");
	
	printf("\t\t-Marks Chart-\n\n");
	for(i=0;i<5;i++){
		if(student[i].mark<0 || student[i].mark>100){
			printf("Student %d\t%s\t%s\tInvalid Mark",i+1,student[i].name,student[i].subject);
		}else{
			printf("Student %d\t%s\t%s\t%.2f",i+1,student[i].name,student[i].subject,student[i].mark);
		}
		printf("\n");
	}
	
	return 0;
}


